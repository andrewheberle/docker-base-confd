FROM registry.gitlab.com/andrewheberle/docker-base:1.5

# Confd environment
ENV CONFD_BACKEND=env \
    CONFD_MAX_FAILCOUNT=5 \
    CONFD_OPTS= \
    CONFD_UPDATE_INTERVAL=30 \
    CONFD_VCS_URL=github.com/kelseyhightower/confd \
    CONFD_VERSION=0.14.0

# Download and compile confd
RUN apk add --update --no-cache --virtual .base-confd-build-deps go git gcc musl-dev && \
    mkdir -p /src/go/src/$CONFD_VCS_URL && \
    git clone --quiet --branch "v$CONFD_VERSION" https://$CONFD_VCS_URL.git /src/go/src/$CONFD_VCS_URL && \
    cd /src/go/src/$CONFD_VCS_URL && \
    GOPATH=/src/go GOOS=linux CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static"' . && \
    mv ./confd /bin/ && \
    chmod +x /bin/confd && \
    apk del .base-confd-build-deps && \
    rm -rf /src

# Copy root fs skelton
COPY root /

ARG BUILD_DATE
ARG VERSION
ARG NAME="docker-base-confd"
ARG DESCRIPTION="Base image with confd"
ARG VCS_URL="https://gitlab.com/andrewheberle/$NAME"
ARG VCS_REF

LABEL name="$NAME" \
      version="$VERSION" \
      architecture="amd64" \
      description="$DESCRIPTION" \
      maintainer="Andrew Heberle ($VCS_URL)" \
      org.label-schema.description="$DESCRIPTION" \
      org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.name="$NAME" \
      org.label-schema.vcs-ref="$VCS_REF" \
      org.label-schema.vcs-url="$VCS_URL" \
      org.label-schema.version="$VERSION" \
      org.label-schema.schema-version="1.0"
